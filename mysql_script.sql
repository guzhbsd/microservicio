use dbeveris;

create table cliente(
idcliente  integer primary key auto_increment not null,
nombre character varying(50) not null,
apellido character varying(50) not null,
direccion character varying(100) not null,
correo character varying(100) not null
);

insert into cliente (nombre,apellido,direccion,correo) values('LUIS','PEREZ','LIMA','luis@mail.com');
insert into cliente (nombre,apellido,direccion,correo) values('ANDREA','SOSA','LIMA','andrea@mail.com');
insert into cliente (nombre,apellido,direccion,correo) values('ROBERTO','JUAREZ','TRUJILLO','roberto@mail.com');
insert into cliente (nombre,apellido,direccion,correo) values('JAVIER','LOZANO','TRUJILLO','javier@mail.com');
insert into cliente (nombre,apellido,direccion,correo) values('CARLOS','RIOS','CHICLAYO','carlos@mail.com');
insert into cliente (nombre,apellido,direccion,correo) values('ENRIQUE','VILLA','CAJAMARCA','enrique@mail.com');

select * from cliente;