package com.everis.microservicio.api;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.everis.microservicio.dao.IClienteRepositorio;
import com.everis.microservicio.dto.ClienteModelo;

@Service
public class ClienteApi {
  @Autowired
  private IClienteRepositorio cliente;
  
  public ClienteModelo getById(int id) {
      return cliente.findById(id).orElse(null);
  }
  
  public List<ClienteModelo> listarTodos(){
    return cliente.findAll();
  }
  
  public ClienteModelo guardar(ClienteModelo c) {
    return cliente.saveAndFlush(c);
  }
  
  public void eliminar(ClienteModelo c) {
    cliente.delete(c);
  }
  
}
