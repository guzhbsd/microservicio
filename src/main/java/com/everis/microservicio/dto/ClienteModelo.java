package com.everis.microservicio.dto;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Entity
public class ClienteModelo implements Serializable {
  
  private static final long serialVersionUID = 178541578787878001L;
  @Id
  @GeneratedValue
  private int idcliente;
  private String nombre;
  private String apellido;
  private String direccion;
  private String correo;
  
  public ClienteModelo() {}
  public ClienteModelo(int id, String nombre, String apellido, String direccion, String correo) {
    this.idcliente = id;
    this.nombre = nombre;
    this.apellido = apellido;
    this.direccion = direccion;
    this.correo = correo;
  }

}
