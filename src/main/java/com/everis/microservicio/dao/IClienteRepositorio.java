package com.everis.microservicio.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.everis.microservicio.dto.ClienteModelo;

public interface IClienteRepositorio extends JpaRepository<ClienteModelo, Integer> {

}
