package com.everis.microservicio.controlador;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.everis.microservicio.api.ClienteApi;
import com.everis.microservicio.dto.ClienteModelo;

@RestController
@RequestMapping(value = "/everis/microservicio")
public class ClienteControlador {
  @Autowired
  private ClienteApi servicio;
  
  @RequestMapping(value = "/listarClientes", method = RequestMethod.GET)
  public List<ClienteModelo> listarClientes(){
    return servicio.listarTodos();
  }
  
  @RequestMapping(value = "/guardarCliente", method = RequestMethod.POST)
  public String guardarActualizarCliente(@RequestBody ClienteModelo c) {
    String lMsg = "Se creo el registro";
    servicio.guardar(c);
    return lMsg;
  }
  
  @RequestMapping(value = "/eliminarCliente", method = RequestMethod.DELETE)
  public String eliminarCliente(@RequestBody ClienteModelo c) {
    String lMsg = "Se elimino el registro";
    servicio.eliminar(c);
    return lMsg;
  }
  
}
